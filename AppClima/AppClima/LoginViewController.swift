//
//  LoginViewController.swift
//  AppClima
//
//  Created by Katherine Hurtado on 27/10/17.
//  Copyright © 2017 Katherine Hurtado. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var userNametext: UITextField!
    
    @IBOutlet weak var userImage: UIImageView!
    //MARK:- Actions
    @IBAction func loginButton(_ sender: Any) {
        let username = userNametext.text!;
        let password = passwordText.text!;
        switch (username, password) {
        case ("Kathy","kathy"):
            print ("Bienvenida");
            //llamar a otra pantalla
            performSegue(withIdentifier: "citySegue", sender: self)
        //Ingresa Kathy y cualquier otra cosa
        case ("Kathy", _):
            print ("Contraseña incorrecta");
        default:
            print("Usuario y contraseña incorrectas")
        }
    }
    
    @IBAction func loginInvitadoButton(_ sender: Any) {
    }
    
    //MAR:- View Controller life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
