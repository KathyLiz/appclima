//
//  WheatherByCityViewController.swift
//  AppClima
//
//  Created by Katherine Hurtado on 6/11/17.
//  Copyright © 2017 Katherine Hurtado. All rights reserved.
//

import UIKit

class WheatherByCityViewController: UIViewController {

    @IBOutlet weak var cityText: UITextField!
    @IBOutlet weak var showWeatherLabel: UILabel!
    
    @IBAction func consultarButton(_ sender: Any) {
        getWeatherByLocation(cityText.text!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getWeatherByLocation(_ city: String){
        let key="d422a9f27f07bdba51c67c8655d3d7e3";
        let urlStr="http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=\(key)";
        let url = URL(string: urlStr)
        //Se crea el request
        //El url es opcional y eso se pone con el signo de admiración
        let request = URLRequest(url: url!)
        //Se crea la sesión y la tarea shared permite acceder a la instacia global de todos los objetos share permite hace singleton
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            //Comprueba si existe o no el error
            if let _ = error {
                //SI hay error se imprime
                return
            }
            
            let weatherData = data as! Data
            
            //Trasnformar a Json
            //Try y catch
            do{
                
                let weatherJson = try JSONSerialization.jsonObject(with: weatherData, options: [])
                let weatherDictionary = weatherJson as! NSDictionary
                let weatherArray = weatherDictionary["weather"] as! NSArray
                
                // let nameArray = weatherArray["name"] as! NSArray;
                print ("Dictionary\(weatherDictionary)");
                
                let weather = weatherArray[0] as! NSDictionary
                let weatherDescription = weather["description"] ?? "Ha ocurrido un error"
                //Busca la llave "weather" si no la encuentra imprime "Llave no existe"
                // print(weatherDictionary["weather"] ?? "Llave no existe")
                print(weatherDescription)
                //Envia informacion de un hilo a otro con main se trae la referencia del hilo principal y cn el asyn se envia la info cuando ya está lista
                DispatchQueue.main.async {
                    //los callbacks son funciones que desconocen los elementos que se encuentran fuera de ellos, por lo que se le debe indicar con "self" para que pueda tomar ese valor de ahí
                    self.showWeatherLabel.text = "\(weatherDescription)"
                }
                
                //print(type(of: weather))
            }catch{
                print("Error al parsear el objeto")
            }
            //body permite saber qué tipo de dato es el que llega
            //print(type(of: data))
            //   print(weatherData)
        }
        
        
        task.resume();
    }
    

    

}
