//
//  ByLocationViewController.swift
//  AppClima
//
//  Created by Katherine Hurtado on 27/10/17.
//  Copyright © 2017 Katherine Hurtado. All rights reserved.
//

import UIKit
import CoreLocation
class ByLocationViewController: UIViewController, CLLocationManagerDelegate {
    //MARK:- Outlet
    @IBOutlet weak var mostrarClimaLabel: UILabel!
    
    @IBAction func exitButton(_ sender: Any) {
        //Botón regresar completion es un callback
        dismiss(animated: true, completion: nil)
    }
    //framework para capturar la ubicación desde el dispositivo
    let locationManager = CLLocationManager();
    var didGetLocation = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization();
        
        //muestra la pantalla de autorización para conocer la ubicación del dispositivo
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            
             locationManager.delegate=self;
            //Captura la ubicación del Teléfono
            locationManager.startUpdatingLocation();
           
        }
        

    }
    
    //MARK:-> CLLocationManager
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate;
        if !didGetLocation{
            getWeatherByLocation(lat: (location?.latitude)!, lon: (location?.longitude)!);
            didGetLocation=true;
        }
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:->Actions
    private func getWeatherByLocation(lat:Double, lon:Double){
        let key="d422a9f27f07bdba51c67c8655d3d7e3";
        let urlStr="http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=\(key)";
        let url = URL(string: urlStr)
        //Se crea el request
        //El url es opcional y eso se pone con el signo de admiración
        let request = URLRequest(url: url!)
        //Se crea la sesión y la tarea shared permite acceder a la instacia global de todos los objetos share permite hace singleton
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            //Comprueba si existe o no el error
            if let _ = error {
                //SI hay error se imprime
                return
            }
            
            let weatherData = data as! Data
            
            //Trasnformar a Json
            //Try y catch
            do{
                
                 let weatherJson = try JSONSerialization.jsonObject(with: weatherData, options: [])
                let weatherDictionary = weatherJson as! NSDictionary
                let weatherArray = weatherDictionary["weather"] as! NSArray
                let nombre = weatherDictionary["name"] as! String;
               // let nameArray = weatherArray["name"] as! NSArray;
                print(weatherDictionary["name"] ?? "Llave no existe")
                let weather = weatherArray[0] as! NSDictionary
                let weatherDescription = weather["description"] ?? "Ha ocurrido un error"
                //Busca la llave "weather" si no la encuentra imprime "Llave no existe"
              // print(weatherDictionary["weather"] ?? "Llave no existe")
                print(weatherDescription)
                //Envia informacion de un hilo a otro con main se trae la referencia del hilo principal y cn el asyn se envia la info cuando ya está lista
                DispatchQueue.main.async {
                    //los callbacks son funciones que desconocen los elementos que se encuentran fuera de ellos, por lo que se le debe indicar con "self" para que pueda tomar ese valor de ahí
                    self.mostrarClimaLabel.text = "\(weatherDescription) in \(nombre)"
                }
                
                //print(type(of: weather))
            }catch{
                print("Error al parsear el objeto")
            }
            //body permite saber qué tipo de dato es el que llega
            //print(type(of: data))
         //   print(weatherData)
        }
        
        
        task.resume();
    }
}
